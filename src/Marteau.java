public class Marteau extends Tapant{

    public Marteau(String nom){
        super(nom);
    }

    @Override
    public void utiliser(){
        System.out.print("Marteaux ");
        super.utiliser();
    }
}