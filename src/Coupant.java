public class Coupant extends MarqueOutils{

    public Coupant(String marque){
        super(marque);
    }


    public void couper(){
        System.out.print(" couper ");
    }

    @Override
    public void utiliser(){
        super.utiliser();
        System.out.print("permet de ");
        this.couper();
        System.out.println();
    }
}
