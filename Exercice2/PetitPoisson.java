public class PetitPoisson{

    private int posX;
    private int posY;
    private int vitesse;


    public PetitPoisson(int posX, int posY, int vitesse){
        this.posX = posX;
        this.posY = posY;
        this.vitesse = vitesse;
    }

    
    public void nage(){
        this.posX += vitesse;
    }
    public Dessin getDessin(){
        Dessin petitDesssin = new Dessin();
        petitDesssin.ajouteChaine(4, 2, "<><", "rouge");
        return petitDesssin;
    }
}