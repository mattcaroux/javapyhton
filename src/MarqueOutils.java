public class MarqueOutils implements Outils{
    
    private String marque;


    public MarqueOutils(String marque) {
        this.marque = marque;
    }


    public String getMarque(){
        return this.marque;
    }

    @Override
    public void utiliser() {
        throw new UnsupportedOperationException("Unimplemented method 'utiliser'");
    }
}