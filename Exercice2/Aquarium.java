import java.util.ArrayList;
import java.util.List;

public class Aquarium{

    private int hauteur;
    private int largeur;
    private Dessin dessin;
    private List<PetitPoisson> petitPoissons;
    private List<Algue> algues;
    private List<Bulle> bulles;
    private List<GrosPoisson> grosPoissons;


    public Aquarium(int hauteur, int largeur, Dessin dessin){
        this.hauteur = hauteur;
        this.largeur = largeur;
        this.dessin = dessin;
        this.petitPoissons = new ArrayList<>();
    }


    public int getHauteur(){
        return this.hauteur;
    }
    public int getLargeur(){
        return this.largeur;
    }
    public Dessin getDessin(){
        return this.dessin;
    }
    public List<PetitPoisson> ajoutePetitPoisson(){
        PetitPoisson bubulle = new PetitPoisson(7, 2, 1);
        PetitPoisson nemo = new PetitPoisson(12, 4, 1);
        PetitPoisson dory = new PetitPoisson(3, 5, 6);
        this.petitPoissons.add(bubulle);
        this.petitPoissons.add(nemo);
        this.petitPoissons.add(dory);
        return petitPoissons;
    }
    public void evolue(){

    }
}