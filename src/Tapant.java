public class Tapant extends MarqueOutils{

    public Tapant(String marque){
        super(marque);
    }


    public void taper(){
        System.out.print(" taper ");
    }

    @Override
    public void utiliser(){
        super.utiliser();
        System.out.print("permettent de ");
        this.taper();
        System.out.println();
    }
}