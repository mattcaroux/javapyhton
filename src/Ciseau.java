public class Ciseau extends Coupant{

    public Ciseau(String nom){
        super(nom);
    }

    @Override
    public void utiliser(){
        System.out.print("Ciseaux ");
        super.utiliser();
    }
}